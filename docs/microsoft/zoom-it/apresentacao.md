# Apresentação -- ZoomIt

O Microsoft ZoomIt nos permite ampliar e congelar a tela, e posteriormente desenhar e rabiscar sobre ela. Veja o vídeo abaixo para ter uma visão geral dela e leia as próximas páginas para aprender a instalá-la e a usá-la.

<iframe width="560" height="315" src="https://www.youtube.com/embed/7XYa9q-nXBQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


- [Instalação](instalacao.md)
- [Ampliação congelada](ampliacao/congelada.md)
- [Desenho](desenho.md)
- [Ampliação interativa](ampliacao/interativa.md)
