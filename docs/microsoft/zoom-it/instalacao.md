# Instalação -- Zoom it

1. Baixe o [Zoom It](https://docs.microsoft.com/en-us/sysinternals/downloads/zoomit) (Clique no link)
2. Abra o arquivo baixado.
3. Extraia o arquivo *zoomit64.exe* para sua área de trabalho.
4. Clique duas vezes (2x) nele (Arquivo *zoomit64.exe*).
5. Marque a opção:
      - [x] *Run ZoomIt when Windows starts*
      - [x] Tradução: Executar o ZoomIt quando o Windows iniciar.

    ![tela inicial do zoomit](../zoom-it/01-inicio.jpg)

6. Clique no botão <kbd>OK</kbd>.


Feito isso, experimente:

- [Ampliação congelada](ampliacao/congelada.md)
- [Desenho](desenho.md)
- [Ampliação interativa](ampliacao/interativa.md)
