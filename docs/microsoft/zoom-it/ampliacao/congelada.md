# Ampliação congelada

!!! warning "Aviso!"
    Recomenda-se a leitura destas instruções em outro dispositivo (celular ou tablet).  

Ampliar e congelar  deixará a tela em um tamanho maior e permitirá que você desenhe sobre ela. Para fazer isso, insira a tecla de atalho:

- <kbd>Ctrl</kbd>+<kbd>1</kbd>

Feito isso, experimente [desenhar na tela](../desenho.md).

Para deixar a ampliação congelada, pressione a tecla <kbd>Esc</kbd>.

